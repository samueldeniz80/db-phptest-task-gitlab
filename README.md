Hello
This repo is to show the CI implementation of DevOps-task MSSQL DB - PHP Server Test.

I created 2 dockerfiles, one for MSSQL Server and second for PHP Apache Webserver. 

gitlab-ci.yml file created to build images from dockerfiles and push the images to AWS ECR.

Terraform is used to create the infrastructure (AWS EKS cluster with suitable ec2 instance type and number).

After the infrastructure setup, with MSSQL DB and  PHP server images kubernetes deployment is made in AWS EKS cluster (kubernetes pods and services are created) 

GitlabCI is used to automaticly build and deploy the project.
##

Each push to the gitlab repo triggers the project pipeline.

# GitLab Agent for GitLab Kubernetes Integration:
In the repository, in the default branch, create this directory at the root:

.gitlab/agents/<agent-name>

In the directory, create a config.yaml file. Ensure the filename ends in .yaml, not .yml.

You can leave the file blank for now, and configure it later.

You must register an agent before you can install the agent in your cluster. To register an agent:

On the top bar, select Main menu > Projects and find your project. If you have an agent configuration file, it must be in this project. Your cluster manifest files should also be in this project.
From the left sidebar, select Infrastructure > Kubernetes clusters.
Select Connect a cluster (agent).
If you want to create a configuration with CI/CD defaults, type a name.
If you already have an agent configuration file, select it from the list.
Select Register an agent.
GitLab generates an access token for the agent. You need this token to install the agent in your cluster.

Securely store the agent access token. A bad actor can use this token to access source code in the agent’s configuration project, access source code in any public project on the GitLab instance, or even, under very specific conditions, obtain a Kubernetes manifest.
Copy the command under Recommended installation method. You need it when you use the one-liner installation method to install the agent in your cluster.

> Install the agent with Helm
To install the agent on your cluster using Helm:

1. Install Helm.
2. In your computer, open a terminal and connect to your cluster.
3. Run the command you copied when you registered your agent with GitLab.

Documantation link: https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#register-the-agent-with-gitlab


# Task

Create two docker containers, one holding a MSSQL database, another one holding a Web-Server offering a pre-defined PHP script. Finally write a Launcher, which starts both containers, so the Web-Server can be called

## Docker Container 1: MSSQL-Server

Write a Dockerfile for the following tasks

- Perform MSSQL Server Installation
- Set password for `SA` to `Un!q@to2023`
- Run MSSQL Service

## Docker Container 2: API

- Install Webserver of your choice
- Install PHP 7.1+
- Install proper driver to connect to MSSQL Server (s. Container 1 above)
- Add the script `QuickDbTest.php` to the web-root folder

## Launcher

Write a launcher, which builds and starts both containers (can be shell scripting or docker-compose)